package com.shop

import com.shop.interfaces.{Item, Items, ItemsInterface, User}

class ItemsService extends ItemsInterface {
  var items = Set.empty[Item]

  def getItems: Items = {
    Items(items.toSeq)
  }

  def addItem(item: Item): String = {
    items += item
    s"Item ${item.name} created."
  }

  def getItem(name: String): Option[Item] = {
    items.find(_.name == name)
  }

  def deleteItem(name: String): String = {
    items.find(_.name == name) foreach { item => items -= item }
    s"Item ${name} deleted."
  }
}