package com.shop.interfaces

final case class Item(name: String, description: String, price: Int, ownerName: String)
final case class Items(items: Seq[Item])


trait ItemsActorInterface {
  final case object GetItems
  final case class AddItem(item: Item)
  final case class GetItem(name: String)
  final case class DeleteItem(name: String)
}

trait ItemsInterface {
  def getItems: Items
  def addItem(item: Item): String
  def getItem(name: String): Option[Item]
  def deleteItem(name: String): String
}