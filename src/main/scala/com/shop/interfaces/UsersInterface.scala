package com.shop.interfaces

final case class User(email: String, name: String, age: Int, countryOfResidence: String,  paymetInfo: PaymentInfo)
final case class PaymentInfo(ownerFullName: String, cardNumber: String, billingAddress: String)
final case class Users(users: Seq[User])

trait UsersActorInterface {
  final case object GetUsers
  final case class CreateUser(user: User)
  final case class GetUser(name: String)
  final case class DeleteUser(name: String)
}

trait UsersInterface {
  def getUsers: Users
  def createUser(user: User): String
  def getUser(name: String): Option[User]
  def deleteUser(name: String): String
}
