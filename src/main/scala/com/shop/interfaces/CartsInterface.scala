package com.shop.interfaces

final case class Cart(name: String, paymentInfo: PaymentInfo, cart: (List[String], Charge))
final case class Carts(carts: Seq[Cart])
final case class Charge(cardNumber: String, amount: Double) {
  def combine(other: Charge): Charge =
    if (cardNumber == other.cardNumber)
      Charge(cardNumber, amount + other.amount)
    else
      throw new Exception("Can't combine charges to different cards")
}


trait CartsActorInterface {
  final case class AddItemToCart(userName: String, itemName: String)
  final case class GetCart(userName: String)
  final case class RemoveItemFromCart(userName: String, itemName: String)
  final case class Charge(userName: String)
}

trait CartsInterface {
  def addItemToCart(userName: String, itemName: String): String
  def getCart(userName: String): Cart
  def removeItemFromCart(userName: String, itemName: String): String
  def charge(userName: String): String
}