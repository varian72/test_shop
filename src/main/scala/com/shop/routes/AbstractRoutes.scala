package com.shop.routes

import akka.actor.{ActorRef, ActorSystem}
import akka.http.javadsl.server.RejectionHandler
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.RouteResult.Complete
import akka.util.Timeout

import scala.concurrent.duration._

trait AbstractRoutes {
  implicit def system: ActorSystem
  def mainActor: ActorRef
  implicit lazy val timeout = Timeout(5.seconds)
}
