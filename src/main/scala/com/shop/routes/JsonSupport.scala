package com.shop.routes

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import com.shop.MainActor.ActionPerformed
import com.shop.interfaces._
import spray.json.DefaultJsonProtocol

trait JsonSupport extends SprayJsonSupport {
  import DefaultJsonProtocol._

  implicit val paymentInfoJsonFormat = jsonFormat3(PaymentInfo)
  implicit val userJsonFormat = jsonFormat5(User)
  implicit val usersJsonFormat = jsonFormat1(Users)
  implicit val itemJsonFormat = jsonFormat4(Item)
  implicit val itemsJsonFormat = jsonFormat1(Items)
  implicit val chargeJsonFormat = jsonFormat2(Charge)
  implicit val cartJsonFormat = jsonFormat3(Cart)
  implicit val cartsJsonFormat = jsonFormat1(Carts)
  implicit val actionPerformedJsonFormat = jsonFormat1(ActionPerformed)
}
