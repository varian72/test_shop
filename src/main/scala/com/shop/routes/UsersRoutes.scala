package com.shop.routes

import akka.event.Logging
import akka.http.scaladsl.model.StatusCodes
import com.shop.interfaces.{User, Users}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.directives.MethodDirectives.get
import akka.http.scaladsl.server.directives.RouteDirectives.complete
import akka.pattern.ask
import com.shop.MainActor.{ActionPerformed, CreateUser, DeleteUser, GetUser, GetUsers}

import scala.concurrent.Future


trait UsersRoutes extends JsonSupport with AbstractRoutes {
  lazy val usersRoutes: Route =
  pathPrefix("users") {
    concat(
      pathEnd {
        concat(
          get {
            val users: Future[Users] =
              (mainActor ? GetUsers).mapTo[Users]
            complete(users)
          },
          post {
            entity(as[User]) { user =>
              val userCreated: Future[ActionPerformed] =
                (mainActor ? CreateUser(user)).mapTo[ActionPerformed]
              onSuccess(userCreated) { performed =>
                log.info("Created user [{}]: {}", user.name, performed.description)
                complete((StatusCodes.Created, performed))
              }
            }
          }
        )
      },
      path(Segment) { name =>
        concat(
          get {
            val maybeUser: Future[Option[User]] =
              (mainActor ? GetUser(name)).mapTo[Option[User]]
            rejectEmptyResponse {
              complete(maybeUser)
            }
          },
          delete {
            val userDeleted: Future[ActionPerformed] =
              (mainActor ? DeleteUser(name)).mapTo[ActionPerformed]
            onSuccess(userDeleted) { performed =>
              log.info("Deleted user [{}]: {}", name, performed.description)
              complete((StatusCodes.OK, performed))
            }
          })
      })
  }
  lazy val log = Logging(system, classOf[UsersRoutes])
}
