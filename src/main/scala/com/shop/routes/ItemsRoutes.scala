package com.shop.routes

import akka.event.Logging
import akka.http.scaladsl.model.StatusCodes
import com.shop.interfaces.{Item, Items}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.directives.MethodDirectives.get
import akka.http.scaladsl.server.directives.RouteDirectives.complete
import akka.pattern.ask
import com.shop.MainActor.{ActionPerformed, AddItem, GetItem, GetItems, DeleteItem}

import scala.concurrent.Future


trait ItemsRoutes extends JsonSupport with AbstractRoutes {
  lazy val itemsRoutes: Route =
    pathPrefix("items") {
      concat(
        pathEnd {
          concat(
            get {
              val items: Future[Items] =
                (mainActor ? GetItems).mapTo[Items]
              complete(items)
            },
            post {
              entity(as[Item]) { item =>
                val itemCreated: Future[ActionPerformed] =
                  (mainActor ? AddItem(item)).mapTo[ActionPerformed]
                onSuccess(itemCreated) { performed =>
                  log.info("Created item [{}]: {}", item.name, performed.description)
                  complete((StatusCodes.Created, performed.description))
                }
              }
            }
          )
        },
        path(Segment) { name =>
          concat(
            get {
              val maybeItem: Future[Option[Item]] =
                (mainActor ? GetItem(name)).mapTo[Option[Item]]
              rejectEmptyResponse {
                complete(maybeItem)
              }
            },
            delete {
              val itemDeleted: Future[ActionPerformed] =
                (mainActor ? DeleteItem(name)).mapTo[ActionPerformed]
              onSuccess(itemDeleted) { performed =>
                log.info("Deleted item [{}]: {}", name, performed.description)
                complete((StatusCodes.OK, performed))
              }
            })
        })
    }
  lazy val log = Logging(system, classOf[UsersRoutes])
}
