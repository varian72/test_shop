package com.shop

import akka.actor.{Actor, ActorLogging, Props}
import akka.http.scaladsl.server.RejectionError
import com.shop.interfaces.{CartsActorInterface, ItemsActorInterface, UsersActorInterface}


object MainActor extends UsersActorInterface with ItemsActorInterface with CartsActorInterface{
  final case class ActionPerformed(description: String)
  def props: Props = Props[MainActor]
}


class MainActor extends Actor with ActorLogging{
  import MainActor._

  var usersService = new UsersService()
  var itemsService = new ItemsService()
//  var cartService = new CartService()


  def receive: Receive = {
    //#Users actions
    case GetUsers =>
      sender() ! usersService.getUsers
    case CreateUser(user)=>
      sender() ! ActionPerformed(usersService.createUser(user))
    case GetUser(name) =>
      sender() ! usersService.getUser(name)
    case DeleteUser(name) =>
      sender() ! ActionPerformed(usersService.deleteUser(name))
    //#Users actions
    //#Items actions
    case GetItems =>
      sender() ! itemsService.getItems
    case AddItem(item)=>
        sender() ! ActionPerformed(itemsService.addItem(item))
    case GetItem(name) =>
      sender() ! itemsService.getItem(name)
    case DeleteItem(name) =>
      sender() ! ActionPerformed(itemsService.deleteItem(name))
    //#Items actions
    //#Carts actions
    //#Carts actions

  }

}
