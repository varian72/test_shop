package com.shop

import com.shop.interfaces.{User, Users, UsersInterface}

class UsersService extends  UsersInterface {
  var users = Set.empty[User]
  def getUsers: Users = {
    Users(users.toSeq)
  }
  def createUser(user: User): String = {
    users += user
    s"User ${user.name} created."
  }
  def getUser(name: String): Option[User] = {
    users.find(_.name == name)
  }
  def deleteUser(name: String): String = {
    users.find(_.name == name) foreach { user => users -= user }
    s"User ${name} deleted."
  }
}
