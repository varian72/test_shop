package com.shop.routes

import akka.actor.ActorRef
import akka.http.scaladsl.marshalling.Marshal
import akka.http.scaladsl.model._
import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.shop.MainActor
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{Matchers, WordSpec}
import akka.http.scaladsl.server.Route
import com.shop.interfaces.{Item, Items}


class ItemsRoutesSpec extends WordSpec with Matchers with ScalaFutures with ScalatestRouteTest with ItemsRoutes {
  override val mainActor: ActorRef =
    system.actorOf(MainActor.props, "mainActor")

  lazy val routes: Route = itemsRoutes
  val item: Item =  Item("test", "testOwner", 420, "loremIpsum")
  val items: Items = Items(List(item))
  //#actual-test
  "ItemsRoutes" should {
    //#testing-post-item
    "be able to add items (POST /items)" in {
      val userEntity = Marshal(item).to[MessageEntity].futureValue

      // using the RequestBuilding DSL:
      val request = Post("/items").withEntity(userEntity)

      request ~> routes ~> check {
        status should ===(StatusCodes.Created)
        contentType should ===(ContentTypes.`text/plain(UTF-8)`)
        entityAs[String] should ===(s"Item ${item.name} created.")
      }
    }
    "return test item  (GET /items)" in {
        val request = HttpRequest(uri = "/items")

        request ~> routes ~> check {
          status should ===(StatusCodes.OK)
          contentType should ===(ContentTypes.`application/json`)
          entityAs[Items] should ===(items)
      }
    }
    "return test item  (GET /items/{name})" in {
      val request = HttpRequest(uri = s"/items/${item.name}")

      request ~> routes ~> check {
        status should ===(StatusCodes.OK)
        contentType should ===(ContentTypes.`application/json`)
        entityAs[Item] should ===(item)
      }
    }
    "be able to remove item  (DELETE /items/{name})" in {
      val request = Delete(uri = s"/items/${item.name}")

      request ~> routes ~> check {
        status should ===(StatusCodes.OK)
        contentType should ===(ContentTypes.`application/json`)
        entityAs[Item] should ===(item)
      }
    "return empty items(GET/items)" in {
      val request = HttpRequest(uri = s"/items")

      request ~> routes ~> check {
        status should ===(StatusCodes.OK)
        contentType should ===(ContentTypes.`application/json`)
        entityAs[Items] should ===(Items(List()))
      }
  }
  }
}

