package com.shop.routes

import akka.actor.ActorRef
import akka.http.scaladsl.marshalling.Marshal
import akka.http.scaladsl.model._
import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.shop.MainActor
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{Matchers, WordSpec}
import akka.http.scaladsl.server.Route
import com.shop.interfaces.{PaymentInfo, User}


class UsersRoutesSpec extends WordSpec with Matchers with ScalaFutures with ScalatestRouteTest with UsersRoutes {
  override val mainActor: ActorRef =
    system.actorOf(MainActor.props, "mainActor")

  lazy val routes: Route = usersRoutes
  val user = User("test@gmail.com", "Kapi", 42, "Ukraine", PaymentInfo("Kapi Test", "1111-2222-3333-4444-5555", "LA, 13Ave"))

  "UserRoutes" should {
    "return no users if no present (GET /users)" in {
      val request = HttpRequest(uri = "/users")

      request ~> routes ~> check {
        status should ===(StatusCodes.OK)
        contentType should ===(ContentTypes.`application/json`)
        entityAs[String] should ===("""{"users":[]}""")
      }
    }
  }

  "be able to add users (POST /users)" in {
    val userEntity = Marshal(user).to[MessageEntity].futureValue
    val request = Post("/users").withEntity(userEntity)

    request ~> routes ~> check {
      status should ===(StatusCodes.Created)
      contentType should ===(ContentTypes.`application/json`)
      entityAs[String] should ===(s"""{"description":"User ${user.name} created."}""")
    }
  }

  "be able to remove users (DELETE /users)" in {
    val request = Delete(uri = s"/users/${user.name}")

    request ~> routes ~> check {
      status should ===(StatusCodes.OK)
      contentType should ===(ContentTypes.`application/json`)
      entityAs[String] should ===(s"""{"description":"User ${user.name} deleted."}""")
    }
  }

  "be able to add user and get it  (POST /users && GET /users/{userName})" in {
    val userEntity = Marshal(user).to[MessageEntity].futureValue
    val postRequest = Post("/users").withEntity(userEntity)
    val getRequest = Get(uri = "/users/")

    postRequest ~> routes ~> check {
      status should ===(StatusCodes.Created)
      contentType should ===(ContentTypes.`application/json`)
      entityAs[String] should ===(s"""{"description":"User ${user.name} created."}""")
    }

    getRequest -> routes -> check {
      status should ===(StatusCodes.Created)
      contentType should ===(ContentTypes.`application/json`)
      entityAs[User] should ===(user)
    }
  }

}

